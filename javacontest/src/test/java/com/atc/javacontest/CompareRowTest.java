package com.atc.javacontest;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import org.apache.log4j.Logger;

import com.atc.javacontest.DataRows.InvalidDataFormatException;
import com.atc.javacontest.correlation.Constants;
import com.atc.javacontest.correlation.Rows;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Тестирует возвращаемые значения.
 * <p>
 * <b>ВНИМАНИЕ:</b> данные тесты проверяют лишь корректность данных,
 * возравщаемых решением (в том числе и правильность расчетов корреляции). Они
 * не проверяют насколько "хорошие" отрезки вернуло решение, и есть ли более
 * хорошее решение.
 */
public class CompareRowTest extends TestCase {

	/** Количество случайных явных проверок корреляции для сложных тестов */
	private static final int EXPLICIT_RANDOM_COORRELATION_CHECKS = 100;
	private static final Logger LOGGER = Logger.getLogger(CompareRowTest.class);

	ICompareRow compareRow;

	public static Test suite() {
		return new TestSuite(CompareRowTest.class);
	}

	@Override
	protected void setUp() throws Exception {
		compareRow = new CompareRowImpl();
		super.setUp();
	}

	public void test0() {
		CorrelationResultIndex result = compareRow.executeTest0(getClass().getResource("/test0/rows.csv"));
		assertEquals("Cлишком малая корреляция", true, 0.999999 < result.correlation);
		assertTrue("Неверное смещение", Math.abs(result.correlationLagIndex - 0) < Constants.EPSILON);
		assertTrue("Неверный множитель", Math.abs(result.correlationMutipleIndex - 1.0) < Constants.EPSILON);
	}

	public void test1() {
		URL resource = getClass().getResource("/test1/rows.csv");
		CorrelationResultIndex result = compareRow.executeTest1(resource);
		checkSingle(result, resource);
	}

	public void test2() {
		URL resource = getClass().getResource("/test2/rows.csv");
		CorrelationResultIndex result = compareRow.executeTest2(resource);
		checkSingle(result, resource);
	}

	public void test3() {
		URL resource = getClass().getResource("/test3/rows.csv");
		CorrelationResultIndex result = compareRow.executeTest3(resource);
		checkSingle(result, resource);
	}

	public void test4() {
		URL resource = getClass().getResource("/test4/rows.csv");
		CorrelationResultIndex result = compareRow.executeTest4(resource);
		checkSingle(result, resource);
	}

	public void test5() {
		URL resource = getClass().getResource("/test5/rows.csv");
		CorrelationResultIndex result = compareRow.executeTest5(resource);
		checkSingle(result, resource);
	}

	public void test6() {
		URL resource = getClass().getResource("/test6/rows.csv");
		CorrelationResultIndex result = compareRow.executeTest6(resource);
		checkSingle(result, resource);
	}

	public void test7() {
		URL resource = getClass().getResource("/test7/rows.csv");
		CorrelationResultIndex result = compareRow.executeTest7(resource);
		checkSingle(result, resource);
	}

	public void test8() {
		URL resource = getClass().getResource("/test8/rows.csv");
		CorrelationResultIndex result = compareRow.executeTest8(resource);
		checkSingle(result, resource);
	}

	public void test9() {
		URL resource = getClass().getResource("/test9/rows.csv");
		CorrelationResultIndex result = compareRow.executeTest9(resource);
		checkSingle(result, resource);
	}

	public void test10() {
		URL resource = getClass().getResource("/test10/rows.csv");
		List<CorrelationResultIndex> result = compareRow.executeTest10(resource);
		checkMultiple(result, resource);
	}

	public void test11() {
		URL resource = getClass().getResource("/test11/rows.csv");
		List<CorrelationResultIndex> result = compareRow.executeTest11(resource);
		checkMultiple(result, resource);
	}

	private void checkSingle(CorrelationResultIndex result, URL resource) {
		try {
			DataRows dataRows = DataRows.readData(resource);
			checkCorrelationResultIndex(result, dataRows);
			if (EXPLICIT_RANDOM_COORRELATION_CHECKS > 0) {
				checkCorrelationExplicitly(result, dataRows);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidDataFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void checkMultiple(List<CorrelationResultIndex> results, URL resource) {
		try {
			DataRows dataRows = DataRows.readData(resource);
			for (final CorrelationResultIndex result : results) {
				checkCorrelationResultIndex(result, dataRows);
			}
			LOGGER.info(results.size() + " ranges found for test file " + resource.getFile());
			if (!results.isEmpty()) {
				final Random random = new Random(1);
				for (int noCheck = 1; noCheck <= EXPLICIT_RANDOM_COORRELATION_CHECKS; noCheck++) {
					final int index = random.nextInt(results.size());
					checkCorrelationExplicitly(results.get(index), dataRows);
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidDataFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void checkCorrelationResultIndex(CorrelationResultIndex result, DataRows dataRows) {
		assertCorrelation(result.correlation);
		assertMutipleIndex(result.correlationMutipleIndex);
		assertLag(result.correlationLagIndex, dataRows);
		assertRangeIndexes((int) result.correlationLagIndex, result.startIndex, result.endIndex, dataRows);
	}

	private void assertCorrelation(double correlation) {
		assertTrue("Корреляция должна лежать в диапазоне от -1 до +1", -1.0 <= correlation && correlation <= 1.0);
	}

	private void assertLag(double correlationLagIndex, DataRows dataRows) {
		final String msg = String.format(
				"Полученное смещение (%.0f) не входит в допустимый для данных рядов диапазон [%d, %d]",
				correlationLagIndex, dataRows.getMinIndex(), dataRows.getMaxLag());
		assertTrue(msg, dataRows.getMinLag() <= correlationLagIndex && correlationLagIndex <= dataRows.getMaxLag());
	}

	private void assertMutipleIndex(double correlationMutipleIndex) {
		assertTrue("Линейный множитель найден некорректно", !Double.isNaN(correlationMutipleIndex));
		assertTrue("Линейный множитель найден некорректно", !Double.isInfinite(correlationMutipleIndex));
		if (Math.abs(correlationMutipleIndex) < 1e-3) {
			LOGGER.warn("correlationMutipleIndex absolute value is too low (" + correlationMutipleIndex + ")");
		}
	}

	private void assertRangeIndexes(int correlationLagIndex, String startIndex, String endIndex, DataRows dataRows) {
		final int start = Integer.parseInt(startIndex);
		final int end = Integer.parseInt(endIndex);
		Rows.Intersection intersection = new Rows(dataRows).getIntersection(correlationLagIndex);
		final int minIndex = dataRows.getMinIndex();
		final int maxIndex = dataRows.getIndexes()[intersection.getLength() - 1];
		assertTrue(String.format("Неверные индексы подотрезка [%d, %d]", start, end), start <= end);
		assertTrue(
				String.format(
						"Подотрезок [%d, %d] лежат не полностью в допустимом для данных рядов и смещения (%d) диапазоне [%d, %d]",
						start, end, correlationLagIndex, minIndex, maxIndex),
				dataRows.getMinIndex() <= start && end <= dataRows.getMaxIndex());
	}

	private void checkCorrelationExplicitly(CorrelationResultIndex result, DataRows dataRows) {
		final int lag = (int) result.correlationLagIndex;
		final int[] indexes = dataRows.getIndexes();
		final int start = Arrays.binarySearch(indexes, Integer.parseInt(result.startIndex));
		final int end = Arrays.binarySearch(indexes, Integer.parseInt(result.endIndex));
		final double[] x = Arrays.copyOfRange(dataRows.getRow1(), start, end + 1);
		final double[] y = Arrays.copyOfRange(dataRows.getRow2(), start - lag, end + 1 - lag);
		final PearsonsCorrelation pearsonsCorrelation = new PearsonsCorrelation();
		final double realCorrelation = pearsonsCorrelation.correlation(x, y);
		final String msg = String.format(Locale.US,
				"[Неверная корреляция] найдено=%f, настоящее значение=%f (" + result + ")", result.correlation,
				realCorrelation);
		assertTrue(msg, Math.abs(result.correlation - realCorrelation) <= Constants.EPSILON);
	}

}
