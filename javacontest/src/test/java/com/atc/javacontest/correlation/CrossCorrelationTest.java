package com.atc.javacontest.correlation;

import java.util.Arrays;
import java.util.Locale;

import com.atc.javacontest.correlation.CrossCorrelation;

import junit.framework.TestCase;

/**
 * Тестирование перекрестной корреляции на простых тестах.
 */
public class CrossCorrelationTest extends TestCase {

	private static final double EPSILON = 1e-11;

	public void testCalculateCrossCorrelation1() {
		final double[] x = { 1.0 };
		final double[] y = { 1.0 };
		final double[] realCrossCorr = { 1.0 };
		final double[] xyCrossCorr = CrossCorrelation.calculateCrossCorrelation(x, y);
		assertTrue(getMessage(xyCrossCorr, realCrossCorr), doubleArraysEquals(realCrossCorr, xyCrossCorr, EPSILON));
	}

	public void testCalculateCrossCorrelation2() {
		final double[] x = { 1.0, 1.0 };
		final double[] y = { 1.0, 1.0 };
		final double[] realCrossCorr = { 1.0, 2.0, 1.0 };
		final double[] xyCrossCorr = CrossCorrelation.calculateCrossCorrelation(x, y);
		assertTrue(getMessage(xyCrossCorr, realCrossCorr), doubleArraysEquals(realCrossCorr, xyCrossCorr, EPSILON));
	}

	public void testCalculateCrossCorrelation3() {
		final double[] x = { 1, 2, 3 };
		final double[] y = { 1, 2, 3, 4, 5 };
		final double[] realCrossCorr = { 5, 14, 26, 20, 14, 8, 3 };
		final double[] xyCrossCorr = CrossCorrelation.calculateCrossCorrelation(x, y);
		assertTrue(getMessage(xyCrossCorr, realCrossCorr), doubleArraysEquals(realCrossCorr, xyCrossCorr, EPSILON));
	}

	private static String getMessage(final double[] test, final double[] real) {
		final String strTest = getNeatArrayString(test);
		final String strReal = getNeatArrayString(real);
		return "Test=" + strTest + ", real=" + strReal;
	}

	private static String getNeatArrayString(double[] array) {
		final StringBuilder sb = new StringBuilder("{");

		boolean first = true;

		for (double elem : array) {
			if (first) {
				first = false;
			} else {
				sb.append(", ");
			}
			sb.append(String.format(Locale.US, "%.2f", elem));
		}

		sb.append("}");

		return sb.toString();
	}

	private static boolean doubleArraysEquals(final double[] a, final double[] b, final double epsilon) {
		assertTrue("Параметр epsilon должен быть неотрицательным числом", !Double.isNaN(epsilon) && epsilon >= 0.0);

		if (a == null && b == null)
			return true;

		if (a == null || b == null)
			return false;

		if (a.length != b.length)
			return false;

		for (int i = 0; i < a.length; i++) {

			final double ai = a[i];
			final double bi = b[i];

			if (ai == bi)
				continue;

			if (Double.isNaN(ai) && Double.isNaN(bi))
				continue;

			if (Double.isNaN(ai) || Double.isNaN(bi))
				return false;

			if (Math.abs(a[i] - b[i]) > epsilon)
				return false;
		}
		return true;
	}

}
