package com.atc.javacontest;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

/**
 * 
 * Класс для работы с рядами данных. Умеет считывать ряды из CSV файла.
 * 
 * @author Sergey Esipenko, 2016
 *
 */
public class DataRows {

	private int[] indexes;
	private double[] row1;
	private double[] row2;

	/**
	 * Считывает данные из CSV файла с заданым URL и возвращает их в виде оъекта
	 * DataRows.
	 * 
	 * @param resource
	 *            URL файла с данными
	 * @return заполненный объект класса DataRows
	 * @throws IOException
	 * @throws URISyntaxException
	 * @throws InvalidDataFormatException
	 */
	public static DataRows readData(URL resource) throws IOException, URISyntaxException, InvalidDataFormatException {
		final List<String> lines = Files.readAllLines(Paths.get(resource.toURI()), StandardCharsets.UTF_8);

		final int nEntires = lines.size();
		final DataRows data = new DataRows();

		data.indexes = new int[nEntires];
		data.row1 = new double[nEntires];
		data.row2 = new double[nEntires];

		int lineIndex = 0;

		for (final String line : lines) {

			// XXX Обход проблемы с другим форматом 11-го теста
			final String delimeter = line.contains(";") ? ";" : ",";

			final String[] tokens = line.split(delimeter);

			if (tokens.length != 3) {
				throw new InvalidDataFormatException("Неверный формат данных");
			}

			data.indexes[lineIndex] = Integer.parseInt(tokens[0].trim());
			data.row1[lineIndex] = Double.parseDouble(tokens[1].trim().replaceAll(",", "."));
			data.row2[lineIndex] = Double.parseDouble(tokens[2].trim().replaceAll(",", "."));

			lineIndex++;
		}

		return data;
	}

	/**
	 * Сигнализирует о неверном формате входных данных
	 * 
	 * @author Sergey Esipenko, 2016
	 *
	 */
	public static class InvalidDataFormatException extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = -620863953188802731L;

		public InvalidDataFormatException(final String msg) {
			super(msg);
		}
	}

	public int[] getIndexes() {
		return indexes.clone();
	}

	public double[] getRow1() {
		return row1.clone();
	}

	public double[] getRow2() {
		return row2.clone();
	}

	public int getMinIndex() {
		return indexes[0];
	}

	public int getMaxIndex() {
		return indexes[indexes.length - 1];
	}

	public int getMinLag() {
		return -getMaxLag(); // используем симметрию
	}

	public int getMaxLag() {
		return indexes.length - 1;
	}

	private DataRows() {
	}

}
