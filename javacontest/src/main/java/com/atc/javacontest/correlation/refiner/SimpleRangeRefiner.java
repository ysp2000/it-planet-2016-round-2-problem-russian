package com.atc.javacontest.correlation.refiner;

import com.atc.javacontest.correlation.Constants;
import com.atc.javacontest.correlation.CorrelationRange;
import com.atc.javacontest.correlation.RangeCorrelationCalculator;

/**
 * 
 * Простой улучшитель. Полезен при тонкой подстройки отрезка. Пытается сдвинуть
 * каждый из концов не более чем на некоторое значение (по умолчанию
 * {@link SimpleRangeRefiner#DEFAULT_REFINEMENT_LOOK_AHEAD}) и с заданным шагом
 * (по умолчанию {@link SimpleRangeRefiner#DEFAULT_REFINEMENT_STEP})
 * 
 * @author Sergey Esipenko, 2016
 *
 */
public class SimpleRangeRefiner implements IRangeRefiner {

	/**
	 * Шаг смещения конца по умолчанию
	 */
	public static final int DEFAULT_REFINEMENT_STEP = 1;

	/**
	 * Максимальныое значение сдвига конца по умолчанию
	 */
	public static final int DEFAULT_REFINEMENT_LOOK_AHEAD = 7;

	private int refinementStep = DEFAULT_REFINEMENT_STEP;
	private int refinementLookAhead = DEFAULT_REFINEMENT_LOOK_AHEAD;

	/**
	 * Улучшает отрезок путем смещения его концов в заданном диапазоне (задается
	 * методом {@link SimpleRangeRefiner#setRefinementLookAhead}) и с заданным
	 * шагом (задается методом {@link SimpleRangeRefiner#setRefinementStep}).
	 * 
	 * @param calculator
	 *            калькулятор корреляции с заданным текущим смещением для
	 *            быстрого подсчета
	 * @param minRangeSize
	 *            минимальный корректный размер отрезка
	 * @param range
	 *            улучшаемый отрезок (модифицирутеся)
	 */
	@Override
	public void refine(RangeCorrelationCalculator calculator, int minRangeSize, CorrelationRange range) {
		refineStart(calculator, minRangeSize, range);
		refineEnd(calculator, minRangeSize, range);
	}

	/**
	 * Смещает только начало
	 * 
	 * @param calculator
	 *            калькулятор корреляции с заданным текущим смещением для
	 *            быстрого подсчета
	 * @param minRangeSize
	 *            минимальный корректный размер отрезка
	 * @param range
	 *            улучшаемый отрезок (модифицирутеся)
	 */
	private void refineStart(RangeCorrelationCalculator calculator, int minRangeSize, CorrelationRange range) {
		final int minStart = range.getStart() - refinementLookAhead;
		final int maxStart = range.getEnd() + refinementLookAhead;

		for (int newStart = minStart; newStart <= maxStart; newStart += refinementStep) {
			if (newStart < calculator.getMinIndex())
				continue;
			if (range.getEnd() - newStart + 1 < minRangeSize)
				continue;
			final double newCorrelation = calculator.getSubrangeCorrelation(newStart, range.getEnd());
			if (newCorrelation >= range.getCorrelation() - Constants.EPSILON) {
				calculator.process(newStart, range.getEnd(), range);
			}
		}
	}

	/**
	 * Смещает только конец
	 * 
	 * @param calculator
	 *            калькулятор корреляции с заданным текущим смещением для
	 *            быстрого подсчета
	 * @param minRangeSize
	 *            минимальный корректный размер отрезка
	 * @param range
	 *            улучшаемый отрезок (модифицирутеся)
	 */
	private void refineEnd(RangeCorrelationCalculator calculator, int minRangeSize, CorrelationRange range) {
		final int minEnd = range.getEnd() - refinementLookAhead;
		final int maxEnd = range.getEnd() + refinementLookAhead;

		for (int newEnd = maxEnd; newEnd >= minEnd; newEnd -= refinementStep) {
			if (newEnd > calculator.getMaxIndex())
				continue;
			if (newEnd - range.getStart() + 1 < minRangeSize)
				continue;
			final double newCorrelation = calculator.getSubrangeCorrelation(range.getStart(), newEnd);
			if (newCorrelation >= range.getCorrelation() - Constants.EPSILON) {
				calculator.process(range.getStart(), newEnd, range);
			}
		}
	}

	public int getRefinementStep() {
		return refinementStep;
	}

	public void setRefinementStep(int refinementStep) {
		this.refinementStep = refinementStep;
	}

	public int getRefinementLookAhead() {
		return refinementLookAhead;
	}

	public void setRefinementLookAhead(int refinementLookAhead) {
		this.refinementLookAhead = refinementLookAhead;
	}
}
