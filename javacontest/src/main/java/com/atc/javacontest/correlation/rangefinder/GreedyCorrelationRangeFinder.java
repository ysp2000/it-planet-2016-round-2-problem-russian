package com.atc.javacontest.correlation.rangefinder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

import com.atc.javacontest.correlation.Constants;
import com.atc.javacontest.correlation.CorrelationRange;
import com.atc.javacontest.correlation.Rows;
import com.atc.javacontest.correlation.Rows.Intersection;
import com.atc.javacontest.correlation.lag.ILagProposer;
import com.atc.javacontest.correlation.refiner.AdaptiveRangeRefiner;

/**
 * 
 * Жадный поиск отрезков.
 * 
 * @author Sergey Esipenko, 2016
 *
 */
public class GreedyCorrelationRangeFinder extends ALagPrecalculatedCorrelationRangeFinder {

	public GreedyCorrelationRangeFinder(Rows dataRows, ILagProposer lagProposer) {
		super(dataRows, lagProposer);
		setRangeRefiner(new AdaptiveRangeRefiner());
	}

	/**
	 * Поиск подотрезков при заданном смещении. В качестве отрезков возвращается
	 * несколько последовательных отрезков, которые предварительно один за
	 * другим жадно расширен.
	 * 
	 * @param intersection
	 *            текущее пересечение рядов
	 * @param output
	 *            коллекция в которую следует помещать найденные отрезки
	 *            (модифицируется)
	 */
	@Override
	protected void findRangesForGivenIntersection(Intersection intersection, Collection<CorrelationRange> output) {
		if (intersection.getLength() < minRangeSize)
			return;
		CorrelationRange range = new CorrelationRange();
		for (int offset = 0; offset + minRangeSize <= intersection.getLength(); offset = range.getEnd()) {

			// сначала просчитываем отрезок минимальной длины приложенный к
			// концу предыдущего
			rangeCorrelationCalculator.process(offset, offset + minRangeSize - 1, range);

			// пытаемся его максимально нарастить
			rangeRefiner.refine(rangeCorrelationCalculator, minRangeSize, range);

			// если полученный отрезок имеет достаточную корреляцию, добавляем
			// его в ответ
			if (range.getCorrelation() > Constants.COORRELATION_THRESHOLD - Constants.EPSILON) {
				output.add(new CorrelationRange(range));
			}
		}
	}
}
