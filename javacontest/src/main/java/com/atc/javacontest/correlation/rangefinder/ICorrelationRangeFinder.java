package com.atc.javacontest.correlation.rangefinder;

import java.util.List;

import com.atc.javacontest.correlation.CorrelationRange;

/**
 * Интерфейс поиска подотрезков рядов
 * 
 * @author Sergey Esipenko, 2016
 *
 */
public interface ICorrelationRangeFinder {

	/**
	 * Находит подотрезки. Критерии и способы поиска зависят от конкретной
	 * реализации.
	 * 
	 * @return список отрезков
	 */
	public List<CorrelationRange> findRanges();
}
