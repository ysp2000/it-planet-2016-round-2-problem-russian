package com.atc.javacontest.correlation;

import java.util.Random;

import com.atc.javacontest.correlation.Rows.Intersection;

/**
 * Вспомогательный класс. Содержит небольшые вспомогательные методы разного
 * назначения.
 * 
 * @author Sergey Esipenko, 2016
 *
 */
public class Utils {

	private static final Random random = new Random();

	/**
	 * Генерирует случайный подотрезок, размер которого лежит в диапазоне
	 * [minSize, maxSize], для заданного пересечения рядов.
	 * 
	 * @param intersection
	 *            пересечение рядов
	 * @param minSize
	 *            минимальный размер
	 * @param maxSize
	 *            максималный размер
	 * @return
	 */
	public static CorrelationRange getRandomRange(Intersection intersection, int minSize, int maxSize) {
		minSize = Math.max(1, minSize);
		maxSize = Math.min(maxSize, intersection.getLength());
		if (maxSize < minSize) {
			throw new IllegalArgumentException("Минимальная длина превышает максимальную");
		}
		if (minSize > intersection.getLength()) {
			throw new IllegalArgumentException("Длина запрашиваемого отрезка превышает длину пересечения рядов");
		}
		final int size = getRanomIntInRangeUniform(minSize, maxSize);
		return getRandomRange(intersection, size);
	}

	/**
	 * Генерирует случайный подотрезок размера size для заданного пересечения
	 * рядов.
	 * 
	 * @param intersection
	 *            пересечение рядов
	 * @param size
	 *            размер генерируемого подотрезка
	 * @return
	 */
	public static CorrelationRange getRandomRange(Intersection intersection, int size) {
		if (size <= 0) {
			throw new IllegalArgumentException("Размер должен быть положительным числом");
		}
		if (size > intersection.getLength()) {
			throw new IllegalArgumentException("Длина запрашиваемого отрезка превышает длину пересечения рядов");
		}
		int start = getRanomIntInRangeUniform(0, intersection.getLength() - size);
		final CorrelationRange range = new CorrelationRange().lag(intersection.lag).range(start, start + size - 1);
		return range;
	}

	private static int getRanomIntInRangeUniform(int min, int max) {
		return min + random.nextInt(max - min + 1);
	}
}
