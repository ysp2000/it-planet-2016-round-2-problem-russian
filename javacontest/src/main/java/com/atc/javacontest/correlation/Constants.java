package com.atc.javacontest.correlation;

/**
 * Вспомогательный класс. Содержит все необходимые константы.
 * 
 * @author Sergey Esipenko, 2016
 *
 */
public class Constants {

	/** Величина погрешности при сравнении дробных значений */
	public static double EPSILON = 1e-7;

	/** Пороговое значение наличия корреляции */
	public static double COORRELATION_THRESHOLD = 0.3;

}
