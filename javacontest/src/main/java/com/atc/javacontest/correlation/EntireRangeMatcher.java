package com.atc.javacontest.correlation;

import java.util.Arrays;

import org.apache.commons.cli2.validation.InvalidArgumentException;

import com.atc.javacontest.CorrelationResultIndex;
import com.atc.javacontest.DataRows;
import com.atc.javacontest.correlation.LinearCoefficients;

/**
 * 
 * Находит отрезока с наибольшей корреляцией рассматривая ряды целиком
 * 
 * @author Sergey Esipenko, 2016
 *
 */
public class EntireRangeMatcher {

	public static CorrelationRange match(DataRows data) {
		final CorrelationRange result = new CorrelationRange();

		final double[] row1 = data.getRow1();
		final double[] row2 = data.getRow2();

		// Используем взаимную (перекрестную) корреляцию рядов
		final double[] crossCorrelation = CrossCorrelation.calculateCrossCorrelation(row1, row2);

		double maxCorrelationValue = crossCorrelation[0];
		int maxCorrelationIndex = 0;

		// Находим наибольшее значение взаимной корреляции рядов
		for (int i = 1; i < crossCorrelation.length; i++) {
			if (Math.abs(maxCorrelationValue) < Math.abs(crossCorrelation[i])) {
				maxCorrelationValue = crossCorrelation[i];
				maxCorrelationIndex = i;
			}
		}

		// Определяем линейное смещение рядов, соответствующее наибольшей
		// корреляции
		final int maxCorrelationLag = maxCorrelationIndex - row2.length + 1;

		// Определяем длину подпоследовательности с наибольшей корреляцией
		final int maxCorrelationRangeLength = row1.length - Math.abs(maxCorrelationLag);

		// Считаем корреляцию явно (чтобы не париться с правильной нормализацией
		// взаимной корреляции)
		final RangeCorrelationCalculator calculator = new RangeCorrelationCalculator(new Rows(row1, row2));
		calculator.setLag(maxCorrelationLag);
		calculator.process(0, maxCorrelationRangeLength - 1, result);

		return result;
	}

}
