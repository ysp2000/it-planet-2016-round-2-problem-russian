/**
 * 
 */
package com.atc.javacontest.correlation;

import java.util.Locale;

/**
 * Внутренний формат хранения результатов для отрезка
 * 
 * @author Sergey Esipenko, 2016
 *
 */
public class CorrelationRange {

	double correlation;

	int start;
	int end;
	int lag;

	LinearCoefficients linearCorrection;

	public CorrelationRange() {
	}

	public CorrelationRange(CorrelationRange src) {
		this.copy(src);
	}

	public CorrelationRange copy(CorrelationRange range) {
		return correlation(range.correlation).range(range.start, range.end).lag(range.lag)
				.linearCorrection(range.linearCorrection);
	}

	public CorrelationRange correlation(double correlation) {
		this.correlation = correlation;
		return this;
	}

	public CorrelationRange range(int start, int end) {
		this.start = start;
		this.end = end;
		return this;
	}

	public CorrelationRange lag(int lag) {
		this.lag = lag;
		return this;
	}

	public CorrelationRange linearCorrection(LinearCoefficients linearCorrection) {
		this.linearCorrection = linearCorrection;
		return this;
	}

	public int getLength() {
		return end - start + 1;
	}

	public double getCorrelation() {
		return correlation;
	}

	public int getStart() {
		return start;
	}

	public int getEnd() {
		return end;
	}

	public int getAbsoluteStart() {
		return start + Math.max(0, lag);
	}

	public int getAbsoluteEnd() {
		return end + Math.max(0, lag);
	}

	public int getLag() {
		return lag;
	}

	public LinearCoefficients getLinearCorrection() {
		return linearCorrection;
	}

	/**
	 * Рассчет доверительного интервала (см. <a href=
	 * "http://www.ncss.com/wp-content/themes/ncss/pdf/Procedures/PASS/Confidence_Interval_for_Pearsons_Correlation.pdf">
	 * Confidence Interval for Pearson’s Correlation</a>)
	 * 
	 * @param zConfidenceInterval
	 *            размер доверительного интервала для преобразованного значения
	 *            корреляции (по Фишеру)
	 * @return доверительный интервал
	 */
	public ConfidenceInterval getConfidenceInterval(double zConfidenceInterval) {

		final double z = ConfidenceInterval.fisher(correlation);
		final double std = ConfidenceInterval.standardError(getLength());

		final double zl = z - zConfidenceInterval * std;
		final double zr = z + zConfidenceInterval * std;

		final double rl = ConfidenceInterval.fisherInv(zl);
		final double ru = ConfidenceInterval.fisherInv(zr);

		return new ConfidenceInterval(rl, ru);
	}

	/**
	 * Доверительный интервал
	 * 
	 */
	public static class ConfidenceInterval {

		// множитель при доверительной вероятности 95%
		public static double Z_CONFIDENCE_INTERVAL_95 = 1.96;

		// множитель при доверительной вероятности 99%
		public static double Z_CONFIDENCE_INTERVAL_99 = 2.58;

		/** Нижняя граница доверительного интервала */
		public final double lowerBound;

		/** Верхняя граница доверительного интервала */
		public final double upperBound;

		private ConfidenceInterval(double lowerBound, double upperBound) {
			this.lowerBound = lowerBound;
			this.upperBound = upperBound;
		}

		/**
		 * Прямое преобразование Фишера
		 * 
		 * @param r
		 *            корреляция
		 * @return z преобразованное значение
		 */
		private static double fisher(double r) {
			if (r < -1.0)
				r = -1.0 + Constants.EPSILON;
			if (r > 1.0)
				r = 1.0 - Constants.EPSILON;
			return 0.5 * Math.log((1.0 + r) / (1.0 - r));
		}

		/**
		 * Обратное преобразование Фишера
		 * 
		 * @param z
		 *            преобразованное значение
		 * @return r корреляция
		 */
		private static double fisherInv(double z) {
			return (Math.exp(2.0 * z) - 1.0) / (Math.exp(2.0 * z) + 1.0);
		}

		private static double standardError(int n) {
			return 1.0 / Math.sqrt(n - 3.0);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return String.format(Locale.US, "(%.3f, %.3f)", lowerBound, upperBound);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CorrelationRange [correlation=" + correlation + ", start=" + start + ", end=" + end + ", lag=" + lag
				+ ", linearCorrection=" + linearCorrection + "]";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + end;
		result = prime * result + lag;
		result = prime * result + start;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof CorrelationRange))
			return false;
		CorrelationRange other = (CorrelationRange) obj;
		if (end != other.end)
			return false;
		if (lag != other.lag)
			return false;
		if (start != other.start)
			return false;
		return true;
	}
}
