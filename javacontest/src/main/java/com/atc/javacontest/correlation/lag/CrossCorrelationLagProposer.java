package com.atc.javacontest.correlation.lag;

import java.util.ArrayList;
import java.util.List;

import com.atc.javacontest.correlation.Constants;
import com.atc.javacontest.correlation.CrossCorrelation;
import com.atc.javacontest.correlation.Rows;

/**
 * 
 * Предоставляет линейные смещения на основе перекрестной корреляции. Возвращает
 * только те смещения, которым соответвствуют локальные максимумы
 * нормализованной взаимнокорреляционной функции заданных рядов, значения
 * которых превышают заданное пороговое значение.
 * 
 * @author Sergey Esipenko, 2016
 *
 */
public class CrossCorrelationLagProposer implements ILagProposer {

	final double correlationThreshold;

	/**
	 * 
	 * @param correlationThreshold
	 *            пороговое значение нормализованной взаимнокорреляционной
	 *            функции
	 */
	public CrossCorrelationLagProposer(double correlationThreshold) {
		super();
		this.correlationThreshold = correlationThreshold;
	}

	@Override
	public Iterable<Integer> propose(Rows dataRows, int minWindowSize) {

		final double[] crossCorrelationAbs = CrossCorrelation.calculateNormalizedCrossCorrelation(dataRows.getRow1(),
				dataRows.getRow2());
		for (int i = 0; i < crossCorrelationAbs.length; i++)
			crossCorrelationAbs[i] = Math.abs(crossCorrelationAbs[i]);

		final List<Integer> proposedLags = new ArrayList<Integer>();

		for (int lag = 1; lag + 1 < crossCorrelationAbs.length; lag++) {

			final double pv = crossCorrelationAbs[lag - 1];
			final double cv = crossCorrelationAbs[lag];
			final double nv = crossCorrelationAbs[lag + 1];

			// Слишком маленькая кореляция
			if (Math.abs(cv) + Constants.EPSILON < correlationThreshold) {
				continue;
			}

			// Добавить, если нашли локальный максимум
			if (pv + Constants.EPSILON < cv && cv > nv + Constants.EPSILON) {
				proposedLags.add(dataRows.getMinLag() + lag);
			}
		}

		return proposedLags;
	}

}
