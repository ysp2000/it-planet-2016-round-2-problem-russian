package com.atc.javacontest.correlation.rangefinder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.atc.javacontest.correlation.Constants;
import com.atc.javacontest.correlation.CorrelationRange;
import com.atc.javacontest.correlation.RangeCorrelationCalculator;
import com.atc.javacontest.correlation.Rows;
import com.atc.javacontest.correlation.Rows.Intersection;
import com.atc.javacontest.correlation.lag.ILagProposer;
import com.atc.javacontest.correlation.refiner.DummyRangeRefiner;
import com.atc.javacontest.correlation.refiner.IRangeRefiner;

/**
 * Абстрактный класс поиска подотрезков с предподсчетом для заданного смещения.
 * В работе опирается на {@link ILagProposer} для получения списка
 * рассматриваемых смещений и на {@link RangeCorrelationCalculator} для быстрого
 * нахождения корреляции.
 * 
 * @author Sergey Esipenko, 2016
 *
 */
public abstract class ALagPrecalculatedCorrelationRangeFinder implements ICorrelationRangeFinder {

	public static final int DEFAULT_MIN_RANGE_SIZE = 30;
	public static final double DEFAULT_CORRELATION_THRESHOLD = Constants.COORRELATION_THRESHOLD;
	public static final IRangeRefiner DEFAULT_RANGE_REFINER = new DummyRangeRefiner();

	protected final Rows dataRows;
	protected final RangeCorrelationCalculator rangeCorrelationCalculator;

	protected ILagProposer lagProposer;
	protected int minRangeSize = DEFAULT_MIN_RANGE_SIZE;
	protected double correlationThreshold = DEFAULT_CORRELATION_THRESHOLD;
	protected IRangeRefiner rangeRefiner = DEFAULT_RANGE_REFINER;

	public ALagPrecalculatedCorrelationRangeFinder(Rows dataRows, ILagProposer lagProposer) {
		this.dataRows = dataRows;
		this.rangeCorrelationCalculator = new RangeCorrelationCalculator(dataRows);
		this.lagProposer = lagProposer;
	}

	@Override
	public List<CorrelationRange> findRanges() {
		final Set<CorrelationRange> result = new HashSet<CorrelationRange>();
		final Iterable<Integer> proposedLags = lagProposer.propose(dataRows, minRangeSize);
		for (int lag : proposedLags) {
			rangeCorrelationCalculator.setLag(lag);
			final Intersection rowsIntersection = dataRows.getIntersection(lag);
			findRangesForGivenIntersection(rowsIntersection, result);
		}
		return new ArrayList<CorrelationRange>(result);
	}

	public void setRangeRefiner(IRangeRefiner rangeRefiner) {
		this.rangeRefiner = rangeRefiner;
	}

	/**
	 * Поиск подотрезков при заданном смещении. Реализация данного метода
	 * ключевым образом влияет на характер и время работы всего решения.
	 * 
	 * @param intersection
	 *            текущее пересечение рядов
	 * @param output
	 *            коллекция в которую следует помещать найденные отрезки
	 *            (модифицируется)
	 */
	protected abstract void findRangesForGivenIntersection(Intersection intersection,
			Collection<CorrelationRange> output);

	public ILagProposer getLagProposer() {
		return lagProposer;
	}

	public void setLagProposer(ILagProposer lagProposer) {
		this.lagProposer = lagProposer;
	}

	public int getMinRangeSize() {
		return minRangeSize;
	}

	public void setMinRangeSize(int minRangeSize) {
		this.minRangeSize = minRangeSize;
	}

	public double getCorrelationThreshold() {
		return correlationThreshold;
	}

	public void setCorrelationThreshold(double correlationThreshold) {
		this.correlationThreshold = correlationThreshold;
	}

	public IRangeRefiner getRangeRefiner() {
		return rangeRefiner;
	}
}
