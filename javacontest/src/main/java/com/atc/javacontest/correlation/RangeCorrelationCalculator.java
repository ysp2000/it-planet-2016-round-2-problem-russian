package com.atc.javacontest.correlation;

import java.util.Arrays;

import com.atc.javacontest.correlation.Rows.Intersection;

/**
 * Вспомогательный класс для быстрого подсчета корреляции при заданном смещении.
 * Смена смещения происходит за O(n), однако поиск корреляции для любого
 * подотрезка при заданном смещении выполняется всего за O(1).
 * 
 * @author Sergey Esipenko, 2016
 *
 */
public class RangeCorrelationCalculator {

	private static final int MAX_POWER = 2;

	private final Rows dataRows;

	private int lag = 0;
	private int intersectionLength = 0;

	private double[] x, y;
	private double[][][] partialSums;

	public RangeCorrelationCalculator(Rows dataRows) {
		this.dataRows = dataRows;

		final int len = Math.min(dataRows.getRow1().length, dataRows.getRow2().length);
		partialSums = new double[MAX_POWER + 1][MAX_POWER + 1][len + 1];
	}

	/**
	 * Устанавливает текущее смещение. Время работы O(n).
	 * 
	 * @param lag
	 *            смещение
	 */
	public void setLag(final int lag) {
		if (lag < dataRows.getMinLag()) {
			throw new IllegalArgumentException(
					String.format("Слишком сильное смещение влево (%d). Смещение должно лежать в диапазоне [%d, %d]",
							lag, dataRows.getMinLag(), dataRows.getMaxLag()));
		}
		if (lag > dataRows.getMaxLag()) {
			throw new IllegalArgumentException(
					String.format("Слишком сильное смещение вправо (%d). Смещение должно лежать в диапазоне [%d, %d]",
							lag, dataRows.getMinLag(), dataRows.getMaxLag()));
		}
		this.lag = lag;
		this.precalc();
	}

	/**
	 * Возвращает корреляцию для подотрезка от <code>from</code> (включительно)
	 * до <code>to</code> (включительно).
	 * <p>
	 * Индексы <code>from</code> и <code>to</code> задаются относительно
	 * пересечения, то есть должны лежать в диапазоне от <code>0</code>
	 * (включительно) до значения возвращаемого методом
	 * {@link RangeCorrelationCalculator#getIntersectionLength()} (не
	 * включительно).
	 * <p>
	 * Для удобства можно пользоваться методами
	 * {@link RangeCorrelationCalculator#getMinIndex()} и
	 * {@link RangeCorrelationCalculator#getMaxIndex()} для получения
	 * минимального и максимального возможного значения индекса соответственно.
	 * 
	 * @param from
	 *            индекс начала (включительно)
	 * @param to
	 *            индекс конца (включительно)
	 * @return значение корреляции для подотрезка <code>[from, to]</code>
	 */
	public double getSubrangeCorrelation(final int from, final int to) {

		// Для получения используемых в данном методе формул необходимо раскрыть
		// скобки в стандартной формуле корреляции (по Пирсену) и привести
		// подобные слагаемые

		// находим все возможные частичные суммы
		final double[][] s = getPartialSum(from, to);

		final int n = to - from + 1;

		final double sx = s[1][0]; // sum_{i=from...to} (x[i])
		final double sy = s[0][1]; // sum_{i=from...to} (y[i])
		final double sxx = s[2][0]; // sum_{i=from...to} (x[i]^2)
		final double sxy = s[1][1]; // sum_{i=from...to} (x[i] * y[i])
		final double syy = s[0][2]; // sum_{i=from...to} (y[i]^2)

		final double avgx = sx / n; // x среднее
		final double avgy = sy / n; // y среднее

		final double cov = sxy - sx * sy / n; // находим ковариацию
		final double stdx = Math.sqrt(sxx - 2. * avgx * sx + n * avgx * avgx);
		final double stdy = Math.sqrt(syy - 2. * avgy * sy + n * avgy * avgy);

		return cov / (stdx * stdy); // нормируем ковариацию до корреляции
	}

	/**
	 * Возвращает коэффициенты линейной коррекции для приближения значени
	 * второго ряда к значениям первого ряда на подотрезке от <code>from</code>
	 * (включительно) до <code>to</code> (включительно).
	 * <p>
	 * Индексы <code>from</code> и <code>to</code> задаются относительно
	 * пересечения, то есть должны лежать в диапазоне от <code>0</code>
	 * (включительно) до значения возвращаемого методом
	 * {@link RangeCorrelationCalculator#getIntersectionLength()} (не
	 * включительно).
	 * <p>
	 * Для удобства можно пользоваться методами
	 * {@link RangeCorrelationCalculator#getMinIndex()} и
	 * {@link RangeCorrelationCalculator#getMaxIndex()} для получения
	 * минимального и максимального возможного значения индекса соответственно.
	 * 
	 * @param from
	 *            индекс начала (включительно)
	 * @param to
	 *            индекс конца (включительно)
	 * @return коэффициенты линейной коррекции для подотрезка
	 *         <code>[from, to]</code>
	 */
	public LinearCoefficients getLinearCorretction(final int from, final int to) {

		// Для получения используемых в данном методе формул необходимо
		// восползоваться методом наименьших квадратов

		final double[][] s = getPartialSum(from, to);

		final int n = to - from + 1;

		final double sx = s[1][0];
		final double sy = s[0][1];
		final double sxx = s[2][0];
		final double sxy = s[1][1];

		final double aDenominator = n * sxx - sx * sx;

		double slope, offset;

		if (Math.abs(aDenominator) > Constants.EPSILON) {
			slope = (n * sxy - sx * sy) / aDenominator;
			offset = 1.0 / n * (sy - sx * slope);
		} else {
			slope = 1.0;
			offset = (sy - sx) / n;
		}

		return new LinearCoefficients(slope, offset);
	}

	/**
	 * Подсчитывает все характеристики для отрезка <code>from</code>
	 * (включительно) до <code>to</code> (включительно) и заносит их в объект
	 * range.
	 * <p>
	 * Индексы <code>from</code> и <code>to</code> задаются относительно
	 * пересечения, то есть должны лежать в диапазоне от <code>0</code>
	 * (включительно) до значения возвращаемого методом
	 * {@link RangeCorrelationCalculator#getIntersectionLength()} (не
	 * включительно).
	 * <p>
	 * Для удобства можно пользоваться методами
	 * {@link RangeCorrelationCalculator#getMinIndex()} и
	 * {@link RangeCorrelationCalculator#getMaxIndex()} для получения
	 * минимального и максимального возможного значения индекса соответственно.
	 * 
	 * @param from
	 *            индекс начала (включительно)
	 * @param to
	 *            индекс конца (включительно)
	 * @param range
	 *            куда поместить результат (модифицируется)
	 */
	public void process(final int from, final int to, CorrelationRange range) {
		range.start = from;
		range.end = to;
		range.lag = lag;
		range.correlation = getSubrangeCorrelation(from, to);
		range.linearCorrection = getLinearCorretction(from, to);
	}

	public int getMinIndex() {
		return 0;
	}

	public int getMaxIndex() {
		return intersectionLength - 1;
	}

	public int getIntersectionLength() {
		return intersectionLength;
	}

	private double[][] getPartialSum(int from, int to) {
		final double[][] psum = new double[MAX_POWER + 1][MAX_POWER + 1];
		for (int xpow = 0; xpow < partialSums.length; xpow++) {
			for (int ypow = 0; ypow < partialSums[xpow].length; ypow++) {
				psum[xpow][ypow] = getPartialSum(xpow, ypow, from, to);
			}
		}
		return psum;
	}

	private double getPartialSum(final int xpow, final int ypow, final int from, final int to) {
		return partialSums[xpow][ypow][to + 1] - partialSums[xpow][ypow][from];
	}

	/**
	 * Предподсчет частичных сумм
	 */
	private void precalc() {

		final Intersection rowsIntersection = dataRows.getIntersection(lag);

		x = rowsIntersection.getIntersectionRow1();
		y = rowsIntersection.getIntersectionRow2();
		intersectionLength = x.length;

		for (int i = 0; i < intersectionLength; i++) {
			double xpart = 1.0;
			for (int xpow = 0; xpow < partialSums.length; xpow++) {
				double ypart = 1.0;
				for (int ypow = 0; ypow < partialSums[xpow].length; ypow++) {
					partialSums[xpow][ypow][i + 1] = partialSums[xpow][ypow][i] + xpart * ypart;
					ypart *= y[i];
				}
				xpart *= x[i];
			}
		}
	}
}
