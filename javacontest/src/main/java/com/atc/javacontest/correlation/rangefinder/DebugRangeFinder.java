package com.atc.javacontest.correlation.rangefinder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.atc.javacontest.correlation.CorrelationRange;
import com.atc.javacontest.correlation.RangeCorrelationCalculator;
import com.atc.javacontest.correlation.Rows;
import com.atc.javacontest.correlation.lag.ILagProposer;

/**
 * Заглушка для поиска ошибок. Возвращает только один отрезок (с нулевым
 * смещением) и индексами [0, 1]
 * 
 * @author Sergey Esipenko, 2016
 *
 */
public class DebugRangeFinder implements ICorrelationRangeFinder {

	private final RangeCorrelationCalculator rangeCorrelationCalculator;

	public DebugRangeFinder(Rows dataRows) {
		this.rangeCorrelationCalculator = new RangeCorrelationCalculator(dataRows);
	}

	@Override
	public List<CorrelationRange> findRanges() {
		CorrelationRange range = new CorrelationRange();
		rangeCorrelationCalculator.setLag(0);
		rangeCorrelationCalculator.process(0, 1, range);
		return Arrays.asList(range);
	}

}
