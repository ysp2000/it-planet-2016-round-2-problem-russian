package com.atc.javacontest.correlation;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import com.atc.javacontest.DataRows;
import com.atc.javacontest.correlation.Rows.Intersection;

/**
 * Вспомогательный класс для хранения рядов (без учета исходных индексов). Умеет
 * находить пересечение рядов для заданного линейного смещения.
 * 
 * @author Sergey Esipenko, 2016
 *
 */
public class Rows {

	private double[] row1;
	private double[] row2;

	public Rows(DataRows dataRows) {
		this(dataRows.getRow1(), dataRows.getRow2());
	}

	public Rows(double[] row1, double[] row2) {
		this.row1 = row1;
		this.row2 = row2;
	}

	/**
	 * Возвращает пересечение рядов для заданного линейного смещения
	 * 
	 * @param lag
	 *            линейное смещение
	 * @return пересечение рядов
	 */
	public Intersection getIntersection(int lag) {
		return new Intersection(lag);
	}

	public double[] getRow1() {
		return row1.clone();
	}

	public double[] getRow2() {
		return row2.clone();
	}

	public int getMinLag() {
		return -(row2.length - 1);
	}

	public int getMaxLag() {
		return row1.length - 1;
	}

	/**
	 * Пересечение рядов
	 * 
	 * @author Sergey Esipenko, 2016
	 *
	 */
	public class Intersection {

		public final int lag;
		public final int start;
		public final int end;

		private Intersection(int lag) {
			if (lag < getMinLag())
				throw new IllegalArgumentException(
						String.format("Слишком сильное смещение влево. Смещение должно лежать в диапазоне [%d, %d]",
								getMinLag(), getMaxLag()));
			if (lag > getMaxLag())
				throw new IllegalArgumentException(
						String.format("Слишком сильное смещение вправо. Смещение должно лежать в диапазоне [%d, %d]",
								getMinLag(), getMaxLag()));
			this.lag = lag;
			this.start = Math.max(0, lag); // inclusive
			this.end = Math.min(getRow1().length, getRow2().length + lag); // exclusive
		}

		public int getLength() {
			return end - start;
		}

		public double[] getIntersectionRow1(int from, int to) {
			return Arrays.copyOfRange(getIntersectionRow1(), from, to + 1);
		}

		public double[] getIntersectionRow2(int from, int to) {
			return Arrays.copyOfRange(getIntersectionRow2(), from, to + 1);
		}

		public double[] getIntersectionRow1() {
			return Arrays.copyOfRange(getRow1(), start, end);
		}

		public double[] getIntersectionRow2() {
			return Arrays.copyOfRange(getRow2(), start - lag, end - lag);
		}
	}
}
