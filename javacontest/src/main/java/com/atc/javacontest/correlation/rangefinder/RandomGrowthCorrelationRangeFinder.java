package com.atc.javacontest.correlation.rangefinder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

import com.atc.javacontest.correlation.Constants;
import com.atc.javacontest.correlation.CorrelationRange;
import com.atc.javacontest.correlation.Rows;
import com.atc.javacontest.correlation.Utils;
import com.atc.javacontest.correlation.Rows.Intersection;
import com.atc.javacontest.correlation.lag.ILagProposer;
import com.atc.javacontest.correlation.refiner.AdaptiveRangeRefiner;

/**
 * 
 * Рандомизированный жадный поиск отрезков.
 * 
 * @author Sergey Esipenko, 2016
 *
 */
public class RandomGrowthCorrelationRangeFinder extends ALagPrecalculatedCorrelationRangeFinder {

	public static double DEFAULT_SIZE_BIAS_STD = 0.7;
	public static final int DEFAULT_NUM_SAMPLES = 50;

	private double sizeBiasStd = DEFAULT_SIZE_BIAS_STD;
	private int numSamples = DEFAULT_NUM_SAMPLES;

	public RandomGrowthCorrelationRangeFinder(Rows dataRows, ILagProposer lagProposer) {
		super(dataRows, lagProposer);
		setRangeRefiner(new AdaptiveRangeRefiner());
	}

	/**
	 * Поиск подотрезков при заданном смещении. В качестве отрезков возвращается
	 * несколько случайных отрезков, которые предварительно жадно расширяются.
	 * 
	 * @param intersection
	 *            текущее пересечение рядов
	 * @param output
	 *            коллекция в которую следует помещать найденные отрезки
	 *            (модифицируется)
	 */
	@Override
	protected void findRangesForGivenIntersection(Intersection intersection, Collection<CorrelationRange> output) {
		for (int it = 0; it < getNumSamples(); it++) {
			CorrelationRange range = Utils.getRandomRange(intersection,
					getBiasedRanomSize(minRangeSize, intersection.getLength(), sizeBiasStd));
			rangeRefiner.refine(rangeCorrelationCalculator, minRangeSize, range);
			if (range.getCorrelation() > Constants.COORRELATION_THRESHOLD - Constants.EPSILON) {
				output.add(range);
			}
		}
	}

	public int getNumSamples() {
		return numSamples;
	}

	public void setNumSamples(int numSamples) {
		this.numSamples = numSamples;
	}

	private static final Random random = new Random();

	/**
	 * Возвращает размер отрезка. Распределение размеров неравнамерно и смещено
	 * в сторону меньших значений (ближе к min).
	 * 
	 * @param min
	 *            минимальное значение
	 * @param max
	 *            максимальное значение
	 * @param std
	 *            среднеквадратичное отклонение
	 * @return значение от min до max
	 */
	private static int getBiasedRanomSize(int min, int max, double std) {
		double x = Math.abs(random.nextGaussian()) * std * (max - min);
		x = Math.round(x);
		x = Math.max(x, min);
		x = Math.min(x, max);
		return (int) Math.floor(x);
	}
}
