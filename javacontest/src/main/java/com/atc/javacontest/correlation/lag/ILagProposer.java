package com.atc.javacontest.correlation.lag;

import com.atc.javacontest.correlation.Rows;

/**
 * 
 *  Интерфейс поиска линейных смещений между рядами
 *  
 * @author Sergey Esipenko, 2016
 */
public interface ILagProposer {

	/**
	 * Для заданных рядов и минимального размера подотрезка находит
	 * последовательность подходящих линейных смещений
	 * 
	 * @param dataRows
	 *            ряды данных
	 * @param minWindowSize
	 *            минимальный допустимый размер подотрезка
	 * @return последовательность подходящих линейных смещений
	 */
	public Iterable<Integer> propose(Rows dataRows, int minWindowSize);
	
}
