package com.atc.javacontest.correlation;

import java.util.Arrays;

import org.apache.commons.cli2.validation.InvalidArgumentException;
import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.apache.commons.math3.transform.DftNormalization;
import org.apache.commons.math3.transform.FastFourierTransformer;
import org.apache.commons.math3.transform.TransformType;

/**
 * Вспомогательны класс для поиска взаимной (перекрестной) корреляции между
 * двумя рядами
 * 
 * @author Sergey Esipenko
 *
 */
public class CrossCorrelation {

	private static final double EPSILON = 1e-8;

	/**
	 * Находит взаимную (перекрестную) корреляцию между рядами <code>x</code> и
	 * <code>y</code>. Смещается второй ряд.
	 * 
	 * Для быстрого поиска используется Быстрое Преобразование Фурье (см.
	 * <a href=
	 * "http://www.mathworks.com/matlabcentral/fileexchange/43967-circular-cross-correlation-using-fft/content/xcorr_fft.m">
	 * Исходная реализация на языке Matlab от Johannes Schmitz</a>).
	 * 
	 * @param x
	 *            первый ряд
	 * @param y
	 *            второй (смещаемый) ряд
	 * @return взаимнокорреляционная функция
	 */
	public static double[] calculateCrossCorrelation(final double[] x, final double[] y) {
		final FastFourierTransformer fft = new FastFourierTransformer(DftNormalization.STANDARD);

		final int fftLength = nextPowerOfTwo(2 * Math.max(x.length, y.length) - 1);
		final double[] u = new double[fftLength];
		final double[] v = new double[fftLength];

		System.arraycopy(x, 0, u, y.length - 1, x.length);
		System.arraycopy(y, 0, v, 0, y.length);

		final Complex[] xFft = fft.transform(u, TransformType.FORWARD);
		final Complex[] yFft = fft.transform(v, TransformType.FORWARD);
		final Complex[] crossCorrFft = new Complex[xFft.length];
		for (int i = 0; i < crossCorrFft.length; i++) {
			crossCorrFft[i] = xFft[i].multiply(yFft[i].conjugate());
		}
		final Complex[] crossCorr = fft.transform(crossCorrFft, TransformType.INVERSE);
		final double[] result = new double[x.length + y.length - 1];
		for (int i = 0; i < result.length; i++) {
			result[i] = crossCorr[i].getReal();
		}
		return result;
	}

	/**
	 * Находит нормализованную взаимную (перекрестную) корреляцию между рядами
	 * <code>x</code> и <code>y</code>. Смещается второй ряд.
	 * 
	 * @param x
	 *            первый ряд
	 * @param y
	 *            второй (смещаемый) ряд
	 * @return взаимнокорреляционная функция
	 */
	public static double[] calculateNormalizedCrossCorrelation(final double[] x, final double[] y) {
		final double[] nx = normalizeSequence(x);
		final double[] ny = normalizeSequence(y);
		final double[] ncc = calculateCrossCorrelation(nx, ny);

		final double nxNorm = new ArrayRealVector(nx).getNorm();
		final double nyNorm = new ArrayRealVector(ny).getNorm();

		if (Math.abs(nxNorm * nyNorm) > EPSILON)
			for (int i = 0; i < ncc.length; i++)
				ncc[i] /= nxNorm * nyNorm;
		return ncc;
	}

	private static final double[] normalizeSequence(final double[] originalSequence) {
		final double[] normalizedSequence = Arrays.copyOf(originalSequence, originalSequence.length);
		final double meanValue = new Mean().evaluate(originalSequence);
		for (int i = 0; i < normalizedSequence.length; i++)
			normalizedSequence[i] = originalSequence[i] - meanValue;
		return normalizedSequence;
	}

	private static final int nextPowerOfTwo(final int x) {
		if (x == 0)
			return 1;
		if (Integer.bitCount(x) == 1)
			return x;
		return Integer.highestOneBit(x) << 1;
	}
}
