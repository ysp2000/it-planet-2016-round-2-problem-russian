package com.atc.javacontest.correlation.lag;

import java.util.ArrayList;
import java.util.List;

import com.atc.javacontest.correlation.Rows;

/**
 * Находит линейные смещения от минимального до максимального с заданным шагом
 * 
 * @author Sergey Esipenko, 2016
 *
 */
public class StepLagProposer implements ILagProposer {

	public static final int DEFAULT_STEP_SIZE = 10;

	int stepSize;

	public StepLagProposer() {
		this.stepSize = DEFAULT_STEP_SIZE;
	}

	public StepLagProposer(int stepSize) {
		this.stepSize = stepSize;
	}

	@Override
	public Iterable<Integer> propose(Rows dataRows, int minWindowSize) {
		final List<Integer> proposedLags = new ArrayList<Integer>();
		final int minLag = dataRows.getMinLag() + minWindowSize - 1;
		final int maxLag = dataRows.getMaxLag() - minWindowSize;
		for (int lag = minLag; lag <= maxLag; lag += stepSize) {
			proposedLags.add(lag);
		}
		return proposedLags;
	}

}
