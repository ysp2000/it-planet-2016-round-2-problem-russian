package com.atc.javacontest.correlation.refiner;

import com.atc.javacontest.correlation.Constants;
import com.atc.javacontest.correlation.CorrelationRange;
import com.atc.javacontest.correlation.CorrelationRange.ConfidenceInterval;
import com.atc.javacontest.correlation.RangeCorrelationCalculator;

/**
 * 
 * Адаптивный улучшитель (с переменным шагом). Полезен для жадного наращивания
 * отрезка отрезка. Пытается быстро нарастить отрезок с каждой стороны пока это
 * возможно.
 * 
 * @author Sergey Esipenko, 2016
 *
 */
public class AdaptiveRangeRefiner implements IRangeRefiner {

	final IRangeRefiner simpleRefiner = new SimpleRangeRefiner();

	/**
	 * Улучшает отрезок путем максимального наращивания его концов с адаптивно
	 * подстраиваемым шагом.
	 * 
	 * @param calculator
	 *            калькулятор корреляции с заданным текущим смещением для
	 *            быстрого подсчета
	 * @param minRangeSize
	 *            минимальный корректный размер отрезка
	 * @param range
	 *            улучшаемый отрезок (модифицирутеся)
	 */
	@Override
	public void refine(RangeCorrelationCalculator calculator, int minRangeSize, CorrelationRange range) {
		while (true) {
			boolean refined = false;

			// адаптивно меняем шаг
			for (int step = range.getLength(); step > 1; step /= 2) {

				// пытаемся сдвинуть концы с заданным шагом пока это возможно
				while (refineStart(calculator, range, step) || refineEnd(calculator, range, step)) {
					refined = true;
				}
			}

			// не ничего не улучшается, то пора прекратить
			if (!refined)
				break;
		}

		// тонкая подстройка в конце
		simpleRefiner.refine(calculator, minRangeSize, range);
	}

	/**
	 * Смещает только начало
	 * 
	 * @param calculator
	 *            калькулятор корреляции с заданным текущим смещением для
	 *            быстрого подсчета
	 * @param minRangeSize
	 *            минимальный корректный размер отрезка
	 * @param range
	 *            улучшаемый отрезок (модифицирутеся)
	 */
	private boolean refineStart(RangeCorrelationCalculator calculator, CorrelationRange range, int step) {
		CorrelationRange tmp = new CorrelationRange();
		boolean changed = false;

		// двигаемся с заданным шагом пока можем
		for (int newStart = range.getStart() - step; newStart >= calculator.getMinIndex(); newStart -= step) {

			calculator.process(newStart, range.getEnd(), tmp);

			final ConfidenceInterval tmpConfidenceInterval = tmp
					.getConfidenceInterval(ConfidenceInterval.Z_CONFIDENCE_INTERVAL_99);
			final ConfidenceInterval rangeConfidenceInterval = range
					.getConfidenceInterval(ConfidenceInterval.Z_CONFIDENCE_INTERVAL_99);
			/*
			 * Считаем, что отрезок улучшился, если нижняя оценка корреляции
			 * (доверительного интервала) увеличилась. Так мы частично обойдем
			 * проблему погрешности маленьких отрезков, так как иногда выгоднее
			 * взять больший отрезок с чуть меньшей корреляцией, но если мы в
			 * ней больше уверены
			 */
			if (tmpConfidenceInterval.lowerBound > rangeConfidenceInterval.lowerBound - Constants.EPSILON) {
				calculator.process(newStart, range.getEnd(), range);
				changed = true;
			}
		}
		return changed;
	}

	/**
	 * Смещает только конец
	 * 
	 * @param calculator
	 *            калькулятор корреляции с заданным текущим смещением для
	 *            быстрого подсчета
	 * @param minRangeSize
	 *            минимальный корректный размер отрезка
	 * @param range
	 *            улучшаемый отрезок (модифицирутеся)
	 */
	private boolean refineEnd(RangeCorrelationCalculator calculator, CorrelationRange range, int step) {
		CorrelationRange tmp = new CorrelationRange();
		boolean changed = false;

		// двигаемся с заданным шагом пока можем
		for (int newEnd = range.getEnd() + step; newEnd <= calculator.getMaxIndex(); newEnd += step) {
			
			calculator.process(range.getStart(), newEnd, tmp);
			
			final ConfidenceInterval tmpConfidenceInterval = tmp
					.getConfidenceInterval(ConfidenceInterval.Z_CONFIDENCE_INTERVAL_99);
			final ConfidenceInterval rangeConfidenceInterval = range
					.getConfidenceInterval(ConfidenceInterval.Z_CONFIDENCE_INTERVAL_99);
			/*
			 * Считаем, что отрезок улучшился, если нижняя оценка корреляции
			 * (доверительного интервала) увеличилась. Так мы частично обойдем
			 * проблему погрешности маленьких отрезков, так как иногда выгоднее
			 * взять больший отрезок с чуть меньшей корреляцией, но если мы в
			 * ней больше уверены
			 */
			if (tmpConfidenceInterval.lowerBound > rangeConfidenceInterval.lowerBound - Constants.EPSILON) {
				calculator.process(range.getStart(), newEnd, range);
				changed = true;
			}
		}
		return changed;
	}

}
