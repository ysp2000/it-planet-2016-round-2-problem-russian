package com.atc.javacontest.correlation.refiner;

import com.atc.javacontest.correlation.*;

/**
 * 
 * Интерфейс "улучшителя" подотрезков. Объекты, реализующие данный интерфейс
 * предназначены для локального смещения границ подотрезка, с целью улучшения
 * его характеристик. Критерии улучшения, а также характер изменения границ
 * задается конкретной реализацией.
 * 
 * @author Sergey Esipenko, 2016
 *
 */
public interface IRangeRefiner {

	/**
	 * Улучшает отрезок.
	 * 
	 * @param calculator
	 *            калькулятор корреляции с заданным текущим смещением для
	 *            быстрого подсчета
	 * @param minRangeSize
	 *            минимальный корректный размер отрезка
	 * @param range
	 *            улучшаемый отрезок (модифицирутеся)
	 */
	void refine(RangeCorrelationCalculator calculator, int minRangeSize, CorrelationRange range);
}
