package com.atc.javacontest.correlation;

/**
 * Коэффициенты линейного преобразования
 * 
 * @author Sergey Esipenko, 2016
 *
 */
public class LinearCoefficients {

	/** Линейный множитель */
	public final double slope;

	/** Линейное смещение (пересечение прямой с OY) */
	public final double offset;

	public LinearCoefficients(double slope, double offset) {
		this.slope = slope;
		this.offset = offset;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LinearCoefficients [slope=" + slope + ", offset=" + offset + "]";
	}
}
