package com.atc.javacontest.correlation.rangefinder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.atc.javacontest.correlation.CorrelationRange;
import com.atc.javacontest.correlation.Rows;
import com.atc.javacontest.correlation.Rows.Intersection;
import com.atc.javacontest.correlation.lag.ILagProposer;

/**
 * 
 * Переборный поиск отрезков. Перебирает отрезки с заданным шагом.
 * 
 * @author Sergey Esipenko, 2016
 *
 */
public class StepRangeCorrelationFinder extends ALagPrecalculatedCorrelationRangeFinder {

	public static final int DEFAULT_RANGE_SIZE_STEP = 10;
	public static final int DEFAULT_RANGE_OFFSET_STEP = 10;

	private int rangeSizeStep = DEFAULT_RANGE_SIZE_STEP;
	private int rangeOffsetStep = DEFAULT_RANGE_OFFSET_STEP;

	public StepRangeCorrelationFinder(Rows dataRows, ILagProposer lagProposer) {
		super(dataRows, lagProposer);
	}

	/**
	 * Поиск подотрезков при заданном смещении. Отрезки ищутся перебором размера
	 * и позиции с заданным шагом. Сложность работы O((n / step)^2) без учета
	 * процесса тонкого улучшения отрезка.
	 * 
	 * @param intersection
	 *            текущее пересечение рядов
	 * @param output
	 *            коллекция в которую следует помещать найденные отрезки
	 *            (модифицируется)
	 */
	@Override
	protected void findRangesForGivenIntersection(Intersection intersection, Collection<CorrelationRange> output) {
		final int startRangeSize = intersection.getLength() - (intersection.getLength() - minRangeSize) % rangeSizeStep;
		for (int rangeSize = startRangeSize; rangeSize >= minRangeSize; rangeSize -= rangeSizeStep) {
			for (int rangeStart = 0; rangeStart + rangeSize <= intersection
					.getLength(); rangeStart += rangeOffsetStep) {
				final double correlation = rangeCorrelationCalculator.getSubrangeCorrelation(rangeStart,
						rangeStart + rangeSize - 1);
				if (Math.abs(correlation) >= correlationThreshold) {
					CorrelationRange range = new CorrelationRange().correlation(correlation).lag(intersection.lag)
							.range(rangeStart, rangeStart + rangeSize - 1);
					rangeRefiner.refine(rangeCorrelationCalculator, minRangeSize, range);
					rangeCorrelationCalculator.process(range.getStart(), range.getEnd(), range);
					output.add(range);
				}
			}
		}
	}

	public int getRangeSizeStep() {
		return rangeSizeStep;
	}

	public void setRangeSizeStep(int rangeSizeStep) {
		this.rangeSizeStep = rangeSizeStep;
	}

	public int getRangeOffsetStep() {
		return rangeOffsetStep;
	}

	public void setRangeOffsetStep(int rangeOffsetStep) {
		this.rangeOffsetStep = rangeOffsetStep;
	}
}
