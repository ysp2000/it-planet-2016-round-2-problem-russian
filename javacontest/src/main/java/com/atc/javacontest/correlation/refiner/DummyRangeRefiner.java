package com.atc.javacontest.correlation.refiner;

import com.atc.javacontest.correlation.*;

/**
 * 
 * Улучшитель-заглушка. Никак не меняет отрезок.
 * 
 * @author Sergey Esipenko, 2016
 *
 */
public class DummyRangeRefiner implements IRangeRefiner {
	@Override
	public void refine(RangeCorrelationCalculator calculator, int minRangeSize, CorrelationRange range) {
		// Это заглушка: ничего не делаем
	}
}
