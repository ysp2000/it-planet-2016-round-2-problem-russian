package com.atc.javacontest;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;

import com.atc.javacontest.correlation.Constants;
import com.atc.javacontest.correlation.CorrelationRange;
import com.atc.javacontest.correlation.EntireRangeMatcher;
import com.atc.javacontest.correlation.rangefinder.ICorrelationRangeFinder;
import com.atc.javacontest.correlation.rangefinder.RandomGrowthCorrelationRangeFinder;
import com.atc.javacontest.correlation.Rows;
import com.atc.javacontest.correlation.lag.CrossCorrelationLagProposer;
import com.atc.javacontest.correlation.lag.StepLagProposer;
import com.atc.javacontest.correlation.rangefinder.ALagPrecalculatedCorrelationRangeFinder;
import com.atc.javacontest.correlation.rangefinder.DebugRangeFinder;
import com.atc.javacontest.correlation.rangefinder.GreedyCorrelationRangeFinder;
import com.atc.javacontest.correlation.rangefinder.StepRangeCorrelationFinder;
import com.atc.javacontest.DataRows.InvalidDataFormatException;

public class CompareRowImpl implements ICompareRow {

	/**
	 * Перечень доступных стратегий решения сложных тестов (где надо вернуть
	 * список CorrelationResultIndex).
	 * 
	 * <p>
	 * Чтобы найти решение за нормальное время приходится применять что-то более
	 * сложное чем простой перебор, так как даже на отнсосительно зашумленных
	 * рядах, отрезков с корреляцией большей 0.3 по модулю очень много.
	 * 
	 * <p>
	 * <i>Для справки: теоретическа оценка для общего количество отрезков
	 * порядка O(n^3) (n возможных линейных смещений и для каждого n^2 возможных
	 * подотрезков, при n=1000 получаем порядка 10^9 отрезков).</i>
	 * 
	 * @author Sergey Esipenko, 2016
	 *
	 */
	enum SolutionType {

		/**
		 * Стратегия для поиска ошибок. Возвращает только один отрезок (с
		 * нулевым смещением)
		 */
		DEBUG {
			@Override
			ICorrelationRangeFinder getCorrelationRangeFinder(Rows rows) {
				return new DebugRangeFinder(rows);
			}
		},

		/**
		 * Полный перебор. Не следует запускать на 10 и 11 тестах.
		 */
		BRUTE_FORCE {
			@Override
			ICorrelationRangeFinder getCorrelationRangeFinder(Rows rows) {
				StepRangeCorrelationFinder rangeCorrelationFinder = new StepRangeCorrelationFinder(rows,
						new StepLagProposer(1));
				rangeCorrelationFinder.setMinRangeSize(2);
				rangeCorrelationFinder.setRangeOffsetStep(1);
				return rangeCorrelationFinder;
			}
		},

		/**
		 * Умное переборное решение (см. {@link StepRangeCorrelationFinder}).
		 * Для ускорения этого решения, накладываются ограничения на минимальный
		 * размер отрезка (так как подсчет корреляции для маленьких отрезки
		 * имеет большую погрешность, поэтому такие отрезки не интересны с
		 * практической точки зрения). Также поиск ведется с некоторым шагом, и
		 * только для хороших смещений (подробнее см.
		 * {@link CrossCorrelationLagProposer} ). Даже в таком случае перебор
		 * занимает более 20 секунд для 11 теста. При этом находится много
		 * вложенных друг в друга отрезков, что выглядит не очень информативным
		 * с практической точки зрения.
		 */
		SMART_STEP {
			@Override
			ICorrelationRangeFinder getCorrelationRangeFinder(Rows rows) {
				return new StepRangeCorrelationFinder(rows,
						new CrossCorrelationLagProposer(Constants.COORRELATION_THRESHOLD / 2));
			}
		},

		/**
		 * Рандомизированное жадное решение (см.
		 * {@link RandomGrowthCorrelationRangeFinder}), которое является неким
		 * компромисом между просто жадным и переборным решением.
		 */
		RANDOM_GREEDY {
			@Override
			ICorrelationRangeFinder getCorrelationRangeFinder(Rows rows) {
				return new RandomGrowthCorrelationRangeFinder(rows,
						new CrossCorrelationLagProposer(Constants.COORRELATION_THRESHOLD / 2));
			}
		},

		/**
		 * <b>Используется по умолчанию</b>
		 * <p>
		 * Жадное решение для 10 и 11 тестов находит достаточно много отрезков
		 * за разумное время, хотя и не все возможные с корреляцией больше 0.3,
		 * так как оно не предназначено для поиска вложенных отрезков или
		 * отрезков имеющих большую общую часть (которые не особо интересны с
		 * практической точки зрения).
		 * <p>
		 * Жадное решение может работать очень быстро за счет двух эвристик:
		 * <p>
		 * Во-первых, целесообразно рассматривать только "хорошие" линейные
		 * смещения, т.е. смещения, которым соответствуют локальные максимумы
		 * взаимнокорреляционной функции для заданных рядов, причем достаточно
		 * большые по модулю (подробнее см. {@link CrossCorrelationLagProposer}
		 * ).
		 * <p>
		 * Во-вторых, данное решение старается искать не пересекающиеся отрезки
		 * для заданного смещения. Зафиксировав очередное смещение, оно берет
		 * отрезок в начале, максимально его наращивает. На конце полученного
		 * отрезка размещается новый отрезок, котороый также максимально
		 * наращивается и т.д. до конца (см.
		 * {@link GreedyCorrelationRangeFinder}).
		 */
		GREEDY {
			@Override
			ICorrelationRangeFinder getCorrelationRangeFinder(Rows rows) {
				return new GreedyCorrelationRangeFinder(rows,
						new CrossCorrelationLagProposer(Constants.COORRELATION_THRESHOLD / 2));
			}
		};

		abstract ICorrelationRangeFinder getCorrelationRangeFinder(Rows rows);
	};

	public CorrelationResultIndex executeTest0(URL resource) {
		return basicTest(resource);
	}

	public CorrelationResultIndex executeTest1(URL resource) {
		return basicTest(resource);
	}

	public CorrelationResultIndex executeTest2(URL resource) {
		return basicTest(resource);
	}

	public CorrelationResultIndex executeTest3(URL resource) {
		return basicTest(resource);
	}

	public CorrelationResultIndex executeTest4(URL resource) {
		return basicTest(resource);
	}

	public CorrelationResultIndex executeTest5(URL resource) {
		return basicTest(resource);
	}

	public CorrelationResultIndex executeTest6(URL resource) {
		return basicTest(resource);
	}

	public CorrelationResultIndex executeTest7(URL resource) {
		return basicTest(resource);
	}

	public CorrelationResultIndex executeTest8(URL resource) {
		return basicTest(resource);
	}

	public CorrelationResultIndex executeTest9(URL resource) {
		return basicTest(resource);
	}

	public List<CorrelationResultIndex> executeTest10(URL resource) {
		return complexTest(resource, SolutionType.GREEDY);
	}

	public List<CorrelationResultIndex> executeTest11(URL resource) {
		return complexTest(resource, SolutionType.GREEDY);
	}

	/**
	 * Простой тест, в котором необходимо найти один отрезок
	 * 
	 * @param resource
	 *            URL файла с рядами
	 * @return отрезок с наибольшей корреляцией для данных рядов
	 */
	private static CorrelationResultIndex basicTest(URL resource) {
		CorrelationResultIndex correlationResultIndex = new CorrelationResultIndex();

		try {
			final DataRows data = DataRows.readData(resource);
			CorrelationRange correlationRange = EntireRangeMatcher.match(data);
			fillCorrelationResultIndex(correlationResultIndex, correlationRange, data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidDataFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return correlationResultIndex;
	}

	/**
	 * Сложный тест, в котором необходимо найти множество подотрезков. В
	 * зависимости от значения флага useGreedySolution выполняется либо быстрое
	 * жадное решение, либо медленное переборное решение.
	 * 
	 * @param resource
	 *            URL файла с рядами
	 * @param useGreedySolution
	 *            использовать ли жадное решение
	 * @return список подотрезков
	 */
	private static List<CorrelationResultIndex> complexTest(URL resource, SolutionType solutionType) {
		List<CorrelationResultIndex> results = new ArrayList<CorrelationResultIndex>();

		try {
			final DataRows data = DataRows.readData(resource);
			ICorrelationRangeFinder correlationRangeFinder = solutionType.getCorrelationRangeFinder(new Rows(data));
			List<CorrelationRange> ranges = correlationRangeFinder.findRanges();
			for (CorrelationRange r : ranges) {
				if (r.getCorrelation() >= Constants.COORRELATION_THRESHOLD) {
					CorrelationResultIndex correlationResultIndex = new CorrelationResultIndex();
					fillCorrelationResultIndex(correlationResultIndex, r, data);
					results.add(correlationResultIndex);
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidDataFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return results;
	}

	/**
	 * Заполняет ответ на задачу исходя из ответа, представленного во внутреннем
	 * формате (см. {@link CorrelationRange}).
	 * 
	 * @param result
	 *            заполняемый ответ (модифицируется)
	 * @param correlationRange
	 *            ответ во внутреннем формате
	 * @param dataRows
	 *            рядя данных
	 */
	private static void fillCorrelationResultIndex(CorrelationResultIndex result, CorrelationRange correlationRange,
			DataRows dataRows) {
		result.correlation = correlationRange.getCorrelation();
		result.correlationLagIndex = correlationRange.getLag();
		result.correlationMutipleIndex = correlationRange.getLinearCorrection().slope;

		final int[] indexes = dataRows.getIndexes();
		result.startIndex = String.valueOf(indexes[correlationRange.getAbsoluteStart()]);
		result.endIndex = String.valueOf(indexes[correlationRange.getAbsoluteEnd()]);

		// в качестве дополнительного индекса будем использовать доверительный
		// интервал для корреляции (с уверенностью 95%), так как это весьма
		// полезная информация при работе с корреляцией
		result.anotherIndex = correlationRange
				.getConfidenceInterval(CorrelationRange.ConfidenceInterval.Z_CONFIDENCE_INTERVAL_95).toString();
		result.anotherIndexDesc = "Доверительный интервал значения корреляции с доверительной вероятностью 95%";
	}
}
